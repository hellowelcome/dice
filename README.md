# dice

Parse and roll [dice notation](https://en.wikipedia.org/wiki/Dice_notation) strings in
[D](https://dlang.org/). It aspires to support everything that [Roll20](https://wiki.roll20.net/Dice_Reference) does (eventually).

## Installation

Add the dependency via [the usual way with DUB](https://dub.pm/getting_started#adding-deps).

The short version is: add `dependency "dice" version="~>1.0.0"` to your project's `dub.sdl` or `"dependences": { "dice": "~>1.0.0" }` to your project's `dub.json` (depending on which one you have).

## Usage

The package has two public functions:

### roll(input) -> float

Parse a dice notation string and roll it. The notation supports:

1. Dice rolling
   ```d
   roll("1d20"); // Rolls a twenty-sided die
   roll("3d6"); // Rolls 3 six-sided dice
   roll("4dF"); // Rolls 4 FATE/FUDGE dice
   ```
2. Arithmetic (addition, subtraction, division, and multiplication)
   ```d
   roll("(3*2)-9/3"); // Performs basic math
   roll("1d20+8-3"); // Performs math after resolving die roll
   ```
3. Dropping and keeping
   ```d
   roll("6d6d3"); // Drops the lowest 3 rolls
   roll("6d6dl3"); // Same as the preceding roll
   roll("6d6k3"); // Keeps the 3 highest rolls
   roll("6d6kh3"); // Same as the preceding roll
   roll("6d6kl3"); // Keeps the 3 lowest rolls
   roll("{3d6,3d6}k1"); // Rolls two groups of 3 six-sided dice and keeps the highest group
   ```
4. Exploding dice (rolling the maximum on a die causes another roll)
   ```d
   roll("3d6!"); // If any of the 3 d6 rolls result in a 6, roll another one
   ```
5. Math functions
   ```d
   roll("ceil(100/75)d6"); // Rounds up to the nearest integer (2) and rolls that many d6 dice
   roll("floor(1/2)"); // Rounds down to the nearest integer (0)
   roll("round(1/2)"); // Rounds down if less than 0.5 and up if 0.5 or more
   roll("abs(1d6-100)"); // Returns the absolute value of the result
   ```
6. Success/Failure count
   ```d
   roll("4d6>3"); // Number of dice rolled that were greater than 3
   roll("4d6f<3"); // Number of dice rolled that were less than 3
   roll("4d6>3f>5"); // Number of dice rolled that were greater than 3 but less than 6
   roll("3d20=11"); // Number of dice rolled that were equal to 11
   roll("3d20f=19"); // Number of dice rolled that were not equal to 19
   ```
   
### Order of Operations

Parts of the notation are solved in the following order:

1. Math functions (`ceil`, `floor`, `round`, `abs`)
2. Notation inside parentheses (`(2+2)d6`)
3. Multiplication and division (`100*2/10`)
4. Addition and subtraction (`100+10-20`)
5. Grouped dice (`{2d6,2d6}`)
6. Success/failure checks (`4d6>2`)
7. FATE dice (`4dF`)
8. Drop/keep dice (`6d6k3`)
9. Exploding dice (`4d6!`)
10. Regular dice (`1d20`)

### rollDie(sides) -> int

Rolls a die with a number of sides and returns the result

```d
rollDie(20); // Rolls a twenty-sided die
rollDie(6); // Rolls a six-sided die
```
