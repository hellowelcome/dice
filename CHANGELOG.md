# Changelog

## v1.0.0
### 2019-02-12

The initial release. Supports:

- Standard dice rolls
- Arithmetic
- Exploding dice
- Drop/keep
- Math functions (floor/ceil/round/abs)
- Grouped rolls
- FATE/FUDGE dice
- Success/failure counting
