/**
* dice, a dice notation parser and resolver
* Authors: Evan Walsh <evan@hellowelcome.org>
* License: AGPL-3.0
*/

module dice;

import std.algorithm, std.array, std.math, std.random, std.range;
import std.stdio : writefln;
import std.conv : to;
import pegged.grammar;

private mixin(grammar("
Roll:
    Expr     <- Factor (Add / Sub)*
    Add      <- '+' Factor
    Sub      <- '-' Factor
    Factor   <- Primary (Mul / Div)*
    Mul      <- ('*' / 'x') Primary
    Div      <- '/' Primary
    Primary  <- Ceil
              / Floor
              / Round
              / Abs
              / '(' Expr ')'
              / Grouped
              / SFDice
              / FateDice
              / DKDice
              / ExplDice
              / Dice
              / Number
              / Negative
              / Variable

    Number   <- ~([0-9]+)
    Negative <- '-' Primary
    Variable <- identifier
    Dice     <- Number 'd' Number
    ExplDice <- Dice '!'
    DKDice   <- Dice DropKeep
    FateDice <- Number 'dF'
    SFDice   <- Dice (Succ Fail / Succ / Fail)
    Fail     <- ('f>' / 'f<' / 'f=') Expr
    Succ     <- ('>' / '<' / '=') Expr
    DropKeep <- ('dh' / 'dl' / 'd' / 'kh' / 'kl' / 'k') Number
    Floor    <- 'floor(' Expr ')'
    Round    <- 'round(' Expr ')'
    Ceil     <- 'ceil(' Expr ')'
    Abs      <- 'abs(' Expr ')'
    Grouped  <- '{' (Expr ',')* Expr '}' DropKeep
"));

/**
 * Parse a dice notation string and roll it
 *
 * Params:
 *   input = String of dice notation
 *
 * Returns: The result of the roll as a float
 */
float roll(string input)
{
  auto parsed = Roll(input);
  return value(parsed);
}

///
unittest
{
  auto rolled = roll("3d6k2+1d20");
  assert(rolled < 33);
  assert(rolled > 2);

  rolled = roll("1d20+8-3*2");
  assert(rolled < 23);
  assert(rolled > 2);

  rolled = roll("1d20d2");
  assert(rolled == 0);

  rolled = roll("2d20k1");
  assert(rolled < 21);
  assert(rolled > 0);

  rolled = roll("3d10kl2");
  assert(rolled < 21);
  assert(rolled > 1);

  rolled = roll("10d20dh5");
  assert(rolled < 101);
  assert(rolled > 4);

  rolled = roll("ceil(1d6/2)");
  assert(rolled > 0);
  assert(rolled < 4);

  rolled = roll("floor(1d6/2)");
  assert(rolled > -1);
  assert(rolled < 4);

  rolled = roll("round(1d6/2)");
  assert(rolled > 0);
  assert(rolled < 4);

  rolled = roll("abs(1d6-100)");
  assert(rolled > 0);
  assert(rolled < 100);

  rolled = roll("(2000-1999)d20");
  assert(rolled > 0);
  assert(rolled < 21);

  rolled = roll("10d2!");
  assert(rolled > 9);

  rolled = roll("10x20");
  assert(rolled == 200);

  rolled = roll("{3d6,3d6,3d6,3d6}k1");
  assert(rolled > 2);
  assert(rolled < 19);

  rolled = roll("4dF");
  assert(rolled > -5);
  assert(rolled < 5);

  rolled = roll("3d6>3f>5");
  assert(rolled > -1);
  assert(rolled < 4);

  rolled = roll("3d6=4");
  assert(rolled > -1);
  assert(rolled < 4);
}

private float value(ParseTree tree)
{
  switch (tree.name)
  {
  case "Roll":
    return value(tree.children[0]);
  case "Roll.Expr":
    float expression = 0.0;
    foreach (child; tree.children)
    {
      expression += value(child);
    }
    return expression;
  case "Roll.Dice":
    int expression = 0;
    const float amount = value(tree.children[0]);
    const float sides = value(tree.children[1]);

    for (int i = 0; i < amount; i++)
    {
      expression += rollDie(to!int(sides));
    }

    return to!float(expression);
  case "Roll.SFDice":
    return rollSuccessFail(tree);
  case "Roll.FateDice":
    return rollFate(tree);
  case "Roll.DKDice":
    return rollDropKeep(tree);
  case "Roll.ExplDice":
    return rollExploding(tree);
  case "Roll.Grouped":
    return rollGrouped(tree);
  case "Roll.Add":
    return value(tree.children[0]);
  case "Roll.Sub":
    return -value(tree.children[0]);
  case "Roll.Factor":
    float expression = 1.0;
    foreach (child; tree.children)
    {
      expression *= value(child);
    }
    return expression;
  case "Roll.Mul":
    return value(tree.children[0]);
  case "Roll.Div":
    return 1.0 / value(tree.children[0]);
  case "Roll.Primary":
    return value(tree.children[0]);
  case "Roll.Negative":
    return -value(tree.children[0]);
  case "Roll.Number":
    return to!float(tree.matches[0]);
  case "Roll.Ceil":
    return ceil(value(tree.children[0]));
  case "Roll.Floor":
    return floor(value(tree.children[0]));
  case "Roll.Round":
    return round(value(tree.children[0]));
  case "Roll.Abs":
    return abs(value(tree.children[0]));
  default:
    return 0;
  }
}

private float rollSuccessFail(ParseTree tree)
{
  auto dice = tree.children[0];
  auto successFail = tree.children[1];
  const auto highTarget = value(successFail.children[0]);
  const auto highMode = successFail.matches[0];
  auto lowTarget = 0.0;
  auto lowMode = "";

  if (tree.children[$ - 1] != tree.children[1])
  {
    auto secondChild = tree.children[$ - 1];
    lowTarget = value(secondChild.children[0]);
    lowMode = tree.children[$ - 1].matches[0];
  }

  const int amount = to!int(dice.matches[0]);
  const int sides = to!int(dice.matches[2]);
  auto counter = 0.0;

  float checkSuccess(float number, float target, string mode)
  {
    auto counter = 0.0;

    switch (mode)
    {
    case ">":
      if (number > target)
        counter += 1;
      break;
    case "<":
      if (number < target)
        counter += 1;
      break;
    case "=":
      if (number == target)
        counter += 1;
      break;
    case "f>":
      if (number > target)
        counter -= 1;
      break;
    case "f<":
      if (number < target)
        counter -= 1;
      break;
    case "f=":
      if (number == target)
        counter -= 1;
      break;
    default:
      break;
    }

    return counter;
  }

  for (int i = 0; i < amount; i++)
  {
    const auto rolled = rollDie(sides);
    counter += checkSuccess(rolled, highTarget, highMode);
    if (lowTarget > 0)
    {
      counter += checkSuccess(rolled, lowTarget, lowMode);
    }
  }

  return counter;
}

private float rollFate(ParseTree tree)
{
  int expression = 0;
  const float amount = value(tree.children[0]);

  for (int i = 0; i < amount; i++)
    expression += uniform(-1, 2, rndGen);

  return to!float(expression);
}

private float rollDropKeep(ParseTree tree)
{
  auto dice = tree.children[0];
  auto dropKeep = tree.children[1];
  const int amount = to!int(dice.matches[0]);
  const int sides = to!int(dice.matches[2]);
  int[] rolls = new int[0];

  for (int i = 0; i < amount; i++)
    rolls ~= rollDie(sides);

  return handleDropKeep(rolls, dropKeep.matches[0], to!int(value(dropKeep.children[0])));
}

private float rollExploding(ParseTree tree)
{
  const auto amount = to!int(tree.matches[0]);
  const auto sides = to!int(tree.matches[2]);
  int[] rolls = new int[0];
  auto keepRolling = true;
  auto bonusRolls = 0;

  do
  {
    if (rolls.length >= amount)
      bonusRolls -= 1;

    auto latestRoll = rollDie(sides);
    rolls ~= latestRoll;

    if (latestRoll == sides)
      bonusRolls += 1;

    keepRolling = rolls.length < amount || bonusRolls > 0;
  }
  while (keepRolling);

  return to!float(rolls.sum());
}

private float handleDropKeep(int[] rolls, string mode, int amount)
{
  rolls.sort();
  if (rolls.length > amount)
  {
    switch (mode)
    {
    case "k", "kh":
      rolls = rolls.reverse().take(amount);
      break;
    case "kl":
      rolls = rolls.take(amount);
      break;
    case "d", "dl":
      rolls = rolls.drop(rolls.length - amount);
      break;
    case "dh":
      rolls = rolls.dropBack(rolls.length - amount);
      break;
    default:
      break;
    }
  }
  else
  {
    switch (mode)
    {
    case "k", "kh", "kl":
      break;
    case "d", "dh", "dl":
      return 0.0;
    default:
      break;
    }
  }

  return to!float(rolls.sum());
}

private float rollGrouped(ParseTree tree)
{
  int[] rolls = new int[0];
  foreach (child; tree.children)
  {
    if (child.name != "Roll.DropKeep")
    {
      // TODO: Handle floats passing through here?
      rolls ~= to!int(value(child));
    }
  }

  auto dropKeep = tree.children[$ - 1];
  auto mode = dropKeep.matches[0];
  auto amount = to!int(value(dropKeep.children[0]));

  return handleDropKeep(rolls, mode, amount);
}

/**
 * Roll a die with a number of sides
 *
 * Params:
 *   sides = Number of sides on the die to roll
 *
 * Returns: The result of the roll as a float
 */
int rollDie(int sides)
{
  return uniform(1, sides + 1, rndGen);
}

///
unittest
{
  auto rolled = rollDie(6);
  assert(rolled > 0);
  assert(rolled < 7);

  rolled = rollDie(20);
  assert(rolled > 0);
  assert(rolled < 21);
}
