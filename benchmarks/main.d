// Run via: dub -c benchmark

/*
Last run:

Simple: 527 ms, 935 μs, and 3 hnsecs (average: ~52 μs and 7 hnsecs)
Complex: 1 sec, 233 ms, 402 μs, and 1 hnsec (average: ~123 μs and 3 hnsecs)
Exploding: 695 ms, 192 μs, and 3 hnsecs (average: ~69 μs and 5 hnsecs)
Grouped: 1 sec, 516 ms, 823 μs, and 2 hnsecs (average: ~151 μs and 6 hnsecs)
*/

module examples.benchmark;

import std.datetime.stopwatch, std.stdio;
import dice;

private static simpleInput = "3d6+6";
private static complexInput = "20d6+1d8+30d6k17*1337";
private static explodingInput = "20d6!+20d4!";
private static groupedInput = "{2d6!,3d6!,4d6!,5d6!}k2";

///
float rollSimple() {
  return roll(simpleInput);
}

///
float rollComplex() {
  return roll(complexInput);
}

///
float rollExploding() {
  return roll(explodingInput);
}

///
float rollGrouped() {
  return roll(groupedInput);
}

void main() {
  auto times = 10_000;
  auto results = benchmark!(rollSimple, rollComplex, rollExploding, rollGrouped)(times);
  writefln("Simple: %s (average: ~%s)", results[0], results[0] / times);
  writefln("Complex: %s (average: ~%s)", results[1], results[1] / times);
  writefln("Exploding: %s (average: ~%s)", results[2], results[2] / times);
  writefln("Grouped: %s (average: ~%s)", results[3], results[3] / times);
}
